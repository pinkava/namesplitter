<?php

declare(strict_types=1);

namespace xs\Support;

use Nette\Utils\Strings;

/**
 * Tool for splitting full name into surname and first name(s)
 * Copyright (c) 2015 Tom Hnatovsky (http://hnatovsky.cz)
 */

class NameSplitter
{
	/** @var array<string> */
	private array $data = [];

	/** @var array<string> */
	private array $dataDegreeBefore = [];

	/** @var array<string> */
	private array $dataDegreeAfter = [];

	/** @var string */
	private $dataFile = __DIR__ . '/../data.php';

	/** @var string */
	private $dataDegreeBeforeFile = __DIR__ . '/../dataDegreeBefore.php';

	/** @var string */
	private $dataDegreeAfterFile = __DIR__ . '/../dataDegreeAfter.php';


	public function __construct()
	{
		$this->loadData();
		$this->loadDataDegreeBefore();
		$this->loadDataDegreeAfter();
	}


	public function splitName(string $fullName): NameSplitterResult
	{
		$firstNames = [];
		$lastNames = [];
		$degreesBefore = [];
		$degreesAfter = [];

		$fullName = trim($fullName);

		if ($fullName === '') {
			return new NameSplitterResult();
		}

		$parts = explode(' ', $fullName);
		$partCount = count($parts);

		foreach ($parts as $part) {
			$part = $part = rtrim(Strings::trim($part), ',');
			if ($this->isDegreeBefore($part)) {
				$degreesBefore[] = $part;
			} elseif ($this->isDegreeAfter($part)) {
				$degreesAfter[] = $part;
			} elseif ($this->isFirstName($part)) {
				$firstNames[] = $part;
			} else {
				$lastNames[] = $part;
			}
		}

		if (count($lastNames) === 0 && $partCount > 1) {
			$lastNames[] = (string) array_pop($firstNames);
		}

		return new NameSplitterResult(
			$firstNames,
			$lastNames,
			$degreesBefore,
			$degreesAfter,
		);
	}


	private function isFirstName(string $name): bool
	{
		return count($this->data) > 0 && in_array(
			Strings::upper($name),
			$this->data,
			true,
		);
	}


	private function isDegreeBefore(string $name): bool
	{
		return count($this->dataDegreeBefore) > 0 && in_array(
			Strings::upper($name),
			$this->dataDegreeBefore,
			true,
		);
	}


	private function isDegreeAfter(string $name): bool
	{
		return count($this->dataDegreeAfter) > 0 && in_array(
			Strings::upper($name),
			$this->dataDegreeAfter,
			true,
		);
	}


	private function loadData(): void
	{
		if (!file_exists($this->dataFile)) {
			return;
		}

		$this->data = include $this->dataFile;
	}


	private function loadDataDegreeBefore(): void
	{
		if (!file_exists($this->dataDegreeBeforeFile)) {
			return;
		}

		$this->dataDegreeBefore = include $this->dataDegreeBeforeFile;
	}


	private function loadDataDegreeAfter(): void
	{
		if (!file_exists($this->dataDegreeAfterFile)) {
			return;
		}

		$this->dataDegreeAfter = include $this->dataDegreeAfterFile;
	}
}
