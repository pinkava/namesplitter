<?php

declare(strict_types=1);

namespace xs\Support;

class NameSplitterResult
{
	/**
	 * @param array<string> $firstNames
	 * @param array<string> $lastNames
	 * @param array<string> $degreeBefore
	 * @param array<string> $degreeAfter
	 */
	public function __construct(
		private array $firstNames = [],
		private array $lastNames = [],
		private array $degreeBefore = [],
		private array $degreeAfter = [],
	) {
	}


	/**
	 * @return array<string>
	 */
	public function getFirstNames(): array
	{
		return $this->firstNames;
	}


	/**
	 * @return array<string>
	 */
	public function getLastNames(): array
	{
		return $this->lastNames;
	}


	/**
	 * @return array<string>
	 */
	public function getDegreesBefore(): array
	{
		return $this->degreeBefore;
	}


	/**
	 * @return array<string>
	 */
	public function getDegreesAfter(): array
	{
		return $this->degreeAfter;
	}


	public function getFirstName(): string
	{
		return implode(' ', $this->firstNames);
	}


	public function getLastName(): string
	{
		return implode(' ', $this->lastNames);
	}


	public function getDegreeBefore(): string
	{
		return implode(', ', $this->degreeBefore);
	}


	public function getDegreeAfter(): string
	{
		return implode(', ', $this->degreeAfter);
	}
}
