<?php

declare(strict_types=1);

return [
	'BC.',
	'BCA.',
	'ING.',
	'ING. ARCH.',
	'MUDR.',
	'MVDR.',
	'MGA.',
	'MGR.',
	'JUDR.',
	'PHDR.',
	'RNDR.',
	'PHARMDR.',
	'THLIC.',
	'THDR.',
	'DOC.',
	'PROF.',
	'PAEDDR.',
	'DR.',
	'PHMR.',
];
