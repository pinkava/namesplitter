FROM dockette/web:php-82

RUN apt-get update && \
  apt-get install -y \
  unzip

# Install Nette coding-standard and code-checker
RUN composer create-project nette/coding-standard /nette-cs
RUN composer create-project nette/code-checker /nette-cc