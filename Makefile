.PHONY: help

##@ General

help: ## Display help message
	@echo ""
	@echo "*****************************************"
	@echo "***                                   ***"
	@echo "***     This is help for project      ***"
	@echo "***                                   ***"
	@echo "*****************************************"
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\nmake \033[36m<target>\033[0m\n"} /^[a-zA-Z_0-9-]+:.*?##/ { printf "\033[36m%-25s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)



##@ Development

start: start-compose composer-install ## Start developing
	
start-compose:
	docker-compose up -d --build
	
stop: ## Stops the docker stack
	docker-compose down

phpstan: ## Check phpstan
	docker-compose exec webserver composer phpstan

phpunit: ## Check phpunit
	docker-compose exec webserver composer phpunit

phpcs: ## Check phpunit
	docker-compose exec webserver composer phpcs

code-cs: ## Chceck coding standard
	docker-compose exec webserver /nette-cs/ecs fix /srv/{src,tests} --preset php82

code-cc: ## Chceck code
	docker-compose exec webserver php /nette-cc/code-checker -d /srv --fix -l --strict-types

code-quality: code-cs code-cc phpstan ## Chceck code quality


##@ Composer packages

composer-install: ## Run composer install
	docker-compose exec webserver composer install

composer-require: ## Composer require 'make composer-require PACKAGE="package"'
	docker-compose exec webserver composer require ${PARAM} ${PACKAGE}

composer-update: ## Run composer update
	docker-compose exec webserver composer update

composer-outdated: ## Run composer outdated
	docker-compose exec webserver composer outdated