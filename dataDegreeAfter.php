<?php

declare(strict_types=1);

return [
	'PH.D.',
	'TH.D.',
	'CSC.',
	'DRSC.',
	'DR. H. C.',
	'DIS.',
];
