<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use xs\Support\NameSplitter;

final class NameSplitterTest extends TestCase
{
	public function testJustFirstName(): void
	{
		$splitter = new NameSplitter();

		$result = $splitter->splitName('Václav');

		$this->assertEquals('Václav', $result->getFirstName());
		$this->assertEquals('', $result->getLastName());

		$this->assertIsArray($result->getFirstNames());
		$this->assertCount(1, $result->getFirstNames());
		$this->assertEquals('Václav', $result->getFirstNames()[0]);

		$this->assertIsArray($result->getLastNames());
		$this->assertCount(0, $result->getLastNames());
	}


	public function testBothFirstNames(): void
	{
		$splitter = new NameSplitter();

		$result = $splitter->splitName('Jiří Vít');

		$this->assertEquals('Jiří', $result->getFirstName());
		$this->assertEquals('Vít', $result->getLastName());

		$this->assertIsArray($result->getFirstNames());
		$this->assertCount(1, $result->getFirstNames());
		$this->assertEquals('Jiří', $result->getFirstNames()[0]);

		$this->assertIsArray($result->getLastNames());
		$this->assertCount(1, $result->getLastNames());
		$this->assertEquals('Vít', $result->getLastNames()[0]);
	}


	public function testBothNamesWithDegree(): void
	{
		$splitter = new NameSplitter();

		$result = $splitter->splitName('MUDr. Jiří Votruba Procházka');

		$this->assertEquals('Jiří', $result->getFirstName());
		$this->assertEquals('Votruba Procházka', $result->getLastName());
		$this->assertEquals('MUDr.', $result->getDegreeBefore());

		$this->assertIsArray($result->getFirstNames());
		$this->assertCount(1, $result->getFirstNames());
		$this->assertEquals('Jiří', $result->getFirstNames()[0]);

		$this->assertIsArray($result->getLastNames());
		$this->assertCount(2, $result->getLastNames());
		$this->assertEquals('Votruba', $result->getLastNames()[0]);
		$this->assertEquals('Procházka', $result->getLastNames()[1]);
	}


	public function testExoticName(): void
	{
		$splitter = new NameSplitter();

		$result = $splitter->splitName('Ta Duc Trunc');

		$this->assertEquals('Ta Duc', $result->getFirstName());
		$this->assertEquals('Trunc', $result->getLastName());

		$this->assertIsArray($result->getFirstNames());
		$this->assertCount(2, $result->getFirstNames());
		$this->assertEquals('Ta', $result->getFirstNames()[0]);
		$this->assertEquals('Duc', $result->getFirstNames()[1]);

		$this->assertIsArray($result->getLastNames());
		$this->assertCount(1, $result->getLastNames());
		$this->assertEquals('Trunc', $result->getLastNames()[0]);
	}
}
